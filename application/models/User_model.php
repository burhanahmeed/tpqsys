<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	public function getAll($table){
		return $this->db->from($table)->get()->result_array();
	}
	public function getAll_admin($table, $sess){
		return $this->db->from($table)
				->where('uid !=',$sess)
				->get()
				->result_array();
	}

	public function _getUserData_on_session($id,$role){
		

		switch ($role) {
			case '1':
				$fields = array('admin_tbl.id','admin_tbl.no_induk','nama_lengkap','role_tbl.name','alamat','no_telp','jenis_klmn','foto');
				$query = $this->db->limit(1)
					->select($fields)
					->join('role_tbl','role_tbl.id = user_table.role','RIGHT')
					->join('admin_tbl','admin_tbl.uid = user_table.uid')
					->get_where('user_table',array('user_table.uid'=>$id))
					->row_array();
				return $query;
				break;
			
			case '2':
			
				break;

			case '3':
			
				break;
		}
	}

	public function _getUserData($id,$role){
		

		switch ($role) {
			case '1':
				$fields = array('admin_tbl.id','admin_tbl.no_induk','nama_lengkap', 'tempat_lahir','tgl_lahir','alamat','no_telp','jenis_klmn','role','foto');
				$query = $this->db->limit(1)
					->select($fields)
					->join('admin_tbl','admin_tbl.uid = user_table.uid')
					->get_where('user_table',array('user_table.no_induk'=>$id))
					->row_array();
				return $query;
				break;
			
			case '2':
				$fields = array('guru_tbl.id','guru_tbl.no_induk','nama_lengkap', 'tempat_lahir','tgl_lahir','alamat','no_telp','jenis_klmn','role','foto');
				$query = $this->db->limit(1)
					->select($fields)
					->join('guru_tbl','guru_tbl.uid = user_table.uid')
					->get_where('user_table',array('user_table.no_induk'=>$id))
					->row_array();
				return $query;
				break;

			case '3':
				$fields = array('murid_tbl.id','murid_tbl.no_induk','nama','nama_wali','tempat_lahir','tgl_lahir','alamat','no_telp','jenis_klmn','role','foto');
				$query = $this->db->limit(1)
					->select($fields)
					->join('murid_tbl','murid_tbl.uid = user_table.uid')
					->get_where('user_table',array('user_table.no_induk'=>$id))
					->row_array();
				return $query;
				break;
		}
	}

	public function _updateProfile($id,$table,$data){
		
			$this->db->where('id',$id);
			$this->db->update($table,$data);
			$getSome = $this->db
				->limit(1)
				->select('no_induk','uid')
				->get_where($table, $id)
				->row_array();
			$then = array('no_induk'=>$getSome['no_induk']);
			$uid= $getSome['uid'];

			$this->db->where('uid',$uid)
				->update('user_table',$then);
			
	}

	public function _updatePass($id,$data){
		$this->db->where('uid',$id);
		$this->db->update('user_table',$data);
	}

	public function _updatePass_Non_Profile($id,$data){
		$this->db->where('no_induk',$id);
		$this->db->update('user_table',$data);
	}

	public function _addUser($table, $userData, $tableData){
		$this->db->trans_begin();
		# Insert Users
		$query_user = $this->db->insert('user_table', $userData);
		// $errno = $this->db->_error_number();
		// die(var_dump($errno));
		# Get ID
		$cause = array(
			'no_induk' => $userData['no_induk'],
			'u_password' => $userData['u_password']
		);
		$getId = $this->db
			->limit(1)
			->select('uid')
			->get_where('user_table', $cause)
			->row_array()['uid'];
		# Insert table
		$tableData['uid'] = $getId;
		$query_admin = $this->db->insert($table, $tableData);
		
		$this->db->trans_complete();
		if ($this->db->trans_status())
		{
			return 0;
		}
		else
		{
			// die(var_dump($errno));
			return $errno;
		}

	}

	public function _delUser($no,$table){
		$query = $this->db->where('no_induk', $no)
				->delete($table);

		if ($query) {
			$q2 = $this->db->where('no_induk', $no)
					->delete('user_table');
			if ($q2) {
				return true;
			} else return false;
		}
	}
}