<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index_admin extends CI_Controller {
//index of ADMIN page

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Login_model');

		$role = '1';
		if ($this->session->userdata('login')) {
			if ($this->Login_model->checkRole($role)) {
				return true;
			} 
		} else redirect('login');
	}

	public function index()
	{
		$data = array(
			'name'=>$this->session->userdata('nama')
			);

		$this->load->view('admin/template',$data);
	}

	public function profile(){
		$this->load->view('admin/profile_v');
	}

	public function guru(){
		$this->load->view('admin/guru_v');	
	}

	public function murid(){
		$this->load->view('admin/murid_v');		
	}

	public function admin(){
		$this->load->view('admin/admin_v');	
	}

}