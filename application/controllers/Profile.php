<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
	}

	public function index(){
		$uid = $this->session->userdata('uid');
		$role = $this->session->userdata('role');
		$getter = $this->User_model->_getUserData_on_session($uid,$role);

		$output = array('id'=>$getter['id'],
					'no_induk'=>$getter['no_induk'],
					'nama'=>$getter['nama_lengkap'],
					'role'=>$getter['name'],
					'alamat'=>$getter['alamat'],
					'telp'=>$getter['no_telp']);

		echo json_encode($output);

	}

	public function editProfile(){
		$uid = $this->session->userdata('id');
		$role = $this->session->userdata('role');
		$_POST = json_decode(file_get_contents('php://input'),true);
		

		switch ($role) {
			case '1':
			$table = 'admin_tbl';
				//KURANG FOTO
			$array = array('no_induk' =>$_POST['no_induk'] ,
					'nama_lengkap'=> $_POST['nama'],
					'alamat'=> $_POST['alamat'],
					'no_telp'=> $_POST['telp']);
			$set = $this->User_model->_updateProfile($uid,$table,$array);

			if ($set) {
				$out = array('res'=>'gagal',
							'msg'=>'err');
				echo json_encode($out);
			} else {
				$out = array('res'=>'sukses',
							'msg'=>'success');
				echo json_encode($out);
			}

				break;
			
			default:
				# code...
				break;
		}
	}

	public function editPass(){
		$this->form_validation->set_rules('password', 'Password', 'trim|matches[cpassword]|required');
            $this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required');
		$uid = $this->session->userdata('uid');
		// $role = $this->session->userdata('role');
		$_POST = json_decode(file_get_contents('php://input'),true);
		$input = array('u_password'=> $_POST['password']);

		if ($this->form_validation->run()==false) {
			$out = validation_errors();
			echo json_encode(array('res'=> $out,'msg'=>'err'));
		} else{
		
			$set = $this->User_model->_updatePass($uid,$input);
			if ($set) {
				$out = array('msg'=>'err','res'=>'Ada sebuah error!');
				echo json_encode($out);
			} else {
				$out = array('msg'=>'success','res'=>'Ganti password sukses!');
				echo json_encode($out);
			}
		}
	}
}