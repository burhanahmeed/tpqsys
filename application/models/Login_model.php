<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	public function checkUser($usname,$pass){
		// if (is_array($usname) || is_array($pass)) return "notfound";


		$get = array(
			'no_induk' => $usname,
			'u_password' => $pass);
		$query = $this->db
				->limit(1)
				->select('role')
				->get_where('user_table',$get);
		// if ($this->db->_error_number() > 0)	return 'notfound';
		if ($query->num_rows()>0) {
			$val = $query->result_array()[0]['role'];
			return $val;
		} else{
			return 'notfound';
		}
	}

	public function getUser($usname,$role){
		switch ($role) {
			case '1':
				$fields = array(
					'.user_table.uid','id','admin_tbl.no_induk','role','nama_lengkap','alamat','no_telp');
				$query = $this->db->limit(1)
							->select($fields)
							->join('admin_tbl','admin_tbl.uid = user_table.uid','LEFT')
							->get_where('user_table',array('user_table.no_induk'=>$usname))
							->row_array();
					return $query;
				break;
			
			case '2':
				$fields = array(
					'.user_table.uid','id','guru_tbl.no_induk','role','nama_lengkap','alamat','no_telp');
				$query = $this->db->limit(1)
							->select($fields)
							->join('guru_tbl','guru_tbl.uid = user_table.uid','LEFT')
							->get_where('user_table',array('user_table.no_induk'=>$usname))
							->row_array();
					return $query;
				break;

			case '3':

				break;
		}
	}

	public function checkRole($AllowedRole){
		$chk = $this->session->userdata('role');
		if ($chk == $AllowedRole) {
			return true;
		}else return false;
	}

}
