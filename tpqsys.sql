-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 03, 2017 at 09:06 AM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tpqsys`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_tbl`
--

CREATE TABLE `admin_tbl` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `no_induk` varchar(11) NOT NULL,
  `nama_lengkap` varchar(256) NOT NULL,
  `alamat` varchar(256) NOT NULL,
  `no_telp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_tbl`
--

INSERT INTO `admin_tbl` (`id`, `uid`, `no_induk`, `nama_lengkap`, `alamat`, `no_telp`) VALUES
(1, 1, '1234', 'MedFo', 'Jalan jalan No.9', '081213414');

-- --------------------------------------------------------

--
-- Table structure for table `guru_tbl`
--

CREATE TABLE `guru_tbl` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `no_induk` varchar(11) NOT NULL,
  `nama_lengkap` varchar(256) NOT NULL,
  `alamat` varchar(256) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `jenis_klmn` varchar(256) NOT NULL,
  `foto` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru_tbl`
--

INSERT INTO `guru_tbl` (`id`, `uid`, `no_induk`, `nama_lengkap`, `alamat`, `no_telp`, `jenis_klmn`, `foto`) VALUES
(1, 2, '0987', 'Somat', 'Jalan jalan no.0', '12345', 'Laki', '');

-- --------------------------------------------------------

--
-- Table structure for table `murid_tbl`
--

CREATE TABLE `murid_tbl` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `no_induk` int(11) NOT NULL,
  `nama` varchar(256) NOT NULL,
  `nama_wali` varchar(256) NOT NULL,
  `tempat_lahir` varchar(256) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` varchar(256) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `jenis_klmn` varchar(256) NOT NULL,
  `foto` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role_tbl`
--

CREATE TABLE `role_tbl` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_tbl`
--

INSERT INTO `role_tbl` (`id`, `name`) VALUES
(1, 'Administrator'),
(2, 'Guru/Pengajar'),
(3, 'Murid/Santri');

-- --------------------------------------------------------

--
-- Table structure for table `user_table`
--

CREATE TABLE `user_table` (
  `uid` int(11) NOT NULL,
  `no_induk` varchar(11) NOT NULL,
  `u_password` varchar(256) NOT NULL,
  `role` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_table`
--

INSERT INTO `user_table` (`uid`, `no_induk`, `u_password`, `role`, `date_created`) VALUES
(1, '1234', '1234', 1, '2017-02-26 15:50:45'),
(2, '0987', '0987', 2, '2017-02-27 06:05:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_tbl`
--
ALTER TABLE `admin_tbl`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `no_induk` (`no_induk`),
  ADD UNIQUE KEY `uid` (`uid`);

--
-- Indexes for table `guru_tbl`
--
ALTER TABLE `guru_tbl`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `no_induk` (`no_induk`),
  ADD UNIQUE KEY `uid` (`uid`);

--
-- Indexes for table `murid_tbl`
--
ALTER TABLE `murid_tbl`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `no_induk` (`no_induk`),
  ADD UNIQUE KEY `uid` (`uid`);

--
-- Indexes for table `role_tbl`
--
ALTER TABLE `role_tbl`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `user_table`
--
ALTER TABLE `user_table`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `no_induk` (`no_induk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_tbl`
--
ALTER TABLE `admin_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `guru_tbl`
--
ALTER TABLE `guru_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `murid_tbl`
--
ALTER TABLE `murid_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `role_tbl`
--
ALTER TABLE `role_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_table`
--
ALTER TABLE `user_table`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
