<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Login_model');
	}

	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function doLogin(){
		$this->form_validation->set_rules('username', 'No Induk', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_message('required', 'Required %s');
		$_POST = json_decode(file_get_contents('php://input'),true);

			$username = $_POST['username'];
			$pass = $_POST['password'];
			if ($this->form_validation->run()==true) {
				$check = $this->Login_model->checkUser($username,$pass);

				switch ($check) {
					case '1':
						$get_session = $this->Login_model->getUser($username,$check);
						$session = array(
							'uid'=>$get_session['uid'],
							'id'=>$get_session['id'],
							'no_induk'=>$get_session['no_induk'],
							'nama'=>$get_session['nama_lengkap'],
							'alamat'=>$get_session['alamat'],
							'telpon'=>$get_session['no_telp'],
							'role'=>$get_session['role'],
							'login'=> true);
						$this->session->set_userdata($session);
						$send = array('url'=>base_url().'admin/index_admin',
								'text'=>'succ');
						echo json_encode($send);
						break;
					
					case '2':
						$get_session = $this->Login_model->getUser($username,$check);
						$session = array(
							'uid'=>$get_session['uid'],
							'id'=>$get_session['id'],
							'no_induk'=>$get_session['no_induk'],
							'nama'=>$get_session['nama_lengkap'],
							'alamat'=>$get_session['alamat'],
							'telpon'=>$get_session['no_telp'],
							'role'=>$get_session['role'],
							'login'=> true);
						$this->session->set_userdata($session);
						$send = array('url'=>base_url().'guru/index_guru',
								'text'=>'succ');
						echo json_encode($send);
						break;

					case '3':
						$get_session = $this->Login_model->getUser($username,$check);
						$session = array(
							'uid'=>$get_session['uid'],
							'id'=>$get_session['id'],
							'no_induk'=>$get_session['no_induk'],
							'nama'=>$get_session['nama'],
							'wali'=>$get_session['nama_wali'],
							'ttl'=>$get_session['tempat_lahir'].", ".$get_session['tgl_lahir'],
							'alamat'=>$get_session['alamat'],
							'telpon'=>$get_session['no_telp'],
							'role'=>$get_session['role'],
							'login'=> true);
						$this->session->set_userdata($session);
						$send = array('url'=>base_url().'murid/index_murid',
								'text'=>'succ');
						echo json_encode($send);
						break;

					case 'notfound':
						 $send = array('text'=>'err');
						 echo json_encode($send);
						break;

					default:
						$send = array('text'=>'err');
						 echo json_encode($send);
				}
			} else {
				 $send = array('text'=>'err');
						 echo json_encode($send);
			}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
}
