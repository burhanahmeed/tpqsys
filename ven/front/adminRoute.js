var admin = angular.module('admin',['ui.router','angularUtils.directives.dirPagination','ngFileUpload']);

admin.config(function($stateProvider,$urlRouterProvider,$locationProvider){
  $stateProvider.state('dashboard', {
        url:'/',
        templateUrl:BASE_URL+'dashboard/home',
        controller: 'dbCtrl'
    }).state('manajemen_murid', {
        url:'/manajemen_murid',
        templateUrl: BASE_URL+'admin/index_admin/murid',
        controller:'muridCtrl',
    }).state('manajemen_guru', {
        url:'/manajemen_guru',
        templateUrl: BASE_URL+'admin/index_admin/guru',
        controller:'guruCtrl',
        // directive : 'fileModel',
        // service : 'multipartForm'
    }).state('manajemen_admin', {
        url:'/manajemen_admin',
        templateUrl: BASE_URL+'admin/index_admin/admin',
        controller:'admCtrl',
        // directive : 'fileModel',
        // service : 'multipartForm'
    }).state('profile',{
        url:'/profile',
        templateUrl: BASE_URL+'admin/index_admin/profile',
        controller: 'profileCtrl'
    }).state('perGuru',{
        url:'/perguru/:no_induk',
        templateUrl: BASE_URL+'admin/manajemen_sdm/perGuru_view',
        controller: 'perguruCtrl'
    }).state('perMurid',{
        url:'/permurid/:no_induk',
        templateUrl: BASE_URL+'admin/manajemen_sdm/permurid_view',
        controller: 'permuridCtrl'
    }).state('perAdmin',{
        url:'/peradmin/:no_induk',
        templateUrl: BASE_URL+'admin/manajemen_sdm/peradmin_view',
        controller: 'peradminCtrl'
    });
    $urlRouterProvider.otherwise('/')
});
