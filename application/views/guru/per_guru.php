   <div class=" btn back-btn offset-atas btn-back">
        <a ui-sref="manajemen_guru"><span class="glyphicon glyphicon-chevron-left"></span> Kembali</a>
    </div>  
        
 
    <!-- angular view -->
    <div class="row padding-offset">
    <div class="profile-title">
        <h3 style="text-align: center;">Detail {{perguru.nama_lengkap}}</h3>
    </div>

    <div class="profile-content row" style="">
        
        <div class="col-md-4 col-lg-4">
            <div class="foto">
                <img src="<?php echo base_url()?>ven/upload/def.png">
            </div>
        </div>

        <div class=" col-md-8 col-lg-8 "> 
                  <table class="table">
                    <tbody>
                      <tr>
                        <td>Nomor Induk</td>
                        <td>{{perguru.no_induk}}</td>
                      </tr>
                      <tr>
                        <td>Nama Lengkap</td>
                        <td>{{perguru.nama_lengkap}}</td>
                      </tr>
                       <tr>
                        <td>Jenis Kelamin</td>
                        <td>{{perguru.jenisKel}}</td>
                      </tr> 
                       <tr>
                        <td>TTL</td>
                        <td>{{perguru.ttl}}</td>
                      </tr>   
                      <tr>
                        <td>Alamat</td>
                        <td>{{perguru.alamat}}</td>
                      </tr>
                      <tr>
                        <td>Nomor Telepon</td>
                        <td>{{perguru.telp}}</td>
                      </tr>                     
                    </tbody>
                  </table>
        <button class="btn back-btn white" data-target="#edit-data" data-toggle="modal"><span class="glyphicon glyphicon-pencil"></span> Edit</button>
        <button class="btn back-btn white" data-target="#edit-pass" data-toggle="modal"><span class="glyphicon glyphicon-eye-close"></span> Ganti Password</button>
        <button class="btn back-btn white" type="submit" ng-click="delete(perguru.no_induk,perguru.role)"><span class="glyphicon glyphicon-trash"> Hapus</span></button>
    </div>
</div>

<!-- MODAL -->
 <div class="modal fade" id="edit-data" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <form method="POST" name="editItem" role="form" ng-submit="submitEditProfile(profile.uid)">
                  <!--   <input ng-model="editc.id" type="hidden" placeholder="Name" name="inputId" class="form-control" /> -->
                    <!-- <input ng-model="editc.long_url" type="hidden" placeholder="Name" name="inputUrl" class="form-control" /> -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Profile</h4>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label>Foto</label>
                                    <input type="file" name="">
                                    <label>Nomor Induk</label>
                                    <input ng-model="perguru.no_induk"  type="text" placeholder="Nomor Induk" class="form-control" required />
                                    <label>Nama Lengkap</label>
                                    <input type="text" placeholder="Nama Lengkap" class="form-control" ng-model="perguru.nama_lengkap">
                                    <label>Jenis Kelamin</label>
                                    <!-- <input type="text" class="form-control" ng-model="perguru.jenisKel"> -->
                                    <select ng-option="jenisKel.nama for jenisKel in jenisK" ng-model="perguru.jenisKel">
                                    <option ng-repeat="j in jenisK" value="{{j.nama}}">{{j.nama}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" ng-disabled="editItem.$invalid" class="btn btn-primary create-crud">Finish & Save</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- MODAL -->
 <div class="modal fade" id="edit-pass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <form method="POST" name="chgpass" role="form" ng-submit="changePass(perguru.no_induk)">
                  <!--   <input ng-model="editc.id" type="hidden" placeholder="Name" name="inputId" class="form-control" /> -->
                    <!-- <input ng-model="editc.long_url" type="hidden" placeholder="Name" name="inputUrl" class="form-control" /> -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Password</h4>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="password" ng-model="password" class="form-control" name="password" required>
                                    <label>Confirm Password</label>
                                    <input id="pass" type="password" ng-model="cpassword" name="cpassword" class="form-control" required autofocus="true">
                                </div>
                            </div>
                        </div>
                        <button id="cpass" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" ng-disabled="chgpass.$invalid" class="btn btn-primary create-crud">Change</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div id="notif"></div>