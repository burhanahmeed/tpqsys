var log = angular.module('logApps',[]);

log.controller('loginCtrl',function($scope,$http,$rootScope){
	$rootScope.title = "Halaman Masuk";

$scope.loginSubmit = function(){
	$scope.loading = true;
	$http.post('login/doLogin/',
			{'username':$scope.username,
			'password':$scope.password
		}).success(function(data){
			$('#notif').show().html('<div class="alert alert-info" role="alert">Mohon tunggu...</div>');
			var obj = $.parseJSON(JSON.stringify(data));
			// console.log(data);
			// console.log($scope.usname);
			if (obj['text']=='succ') {
				$('#notif').show().html('<div class="alert alert-success" role="alert">Berhasil masuk...</div>');
				setTimeout(function(){window.location= ""+obj['url']+""},400);
			} else if(obj['text']=='err'){
				$('#notif').show().html('<div class="alert alert-danger" role="alert">No induk atau Password salah...<span id="cls" class="close">X</span></div>');
					$('#cls').click(function(){
					$('#notif').hide(400);
					});

			}
		});
}
});