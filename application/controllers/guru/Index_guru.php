<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index_guru extends CI_Controller {
//index of GURU
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Login_model');
	}

	public function index()
	{
		$data = array(
			'name'=>$this->session->userdata('nama')
			);
		$this->load->view('guru/template',$data);
	}

}