<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manajemen_sdm extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
		 $this->load->helper(array('form', 'url'));
	}

	public function perGuru($no){
		$getter = $this->User_model->_getUserData($no,'2');
		$out = array('no_induk'=>$getter['no_induk'],
			'nama_lengkap'=>$getter['nama_lengkap'],
			'ttl'=>$getter['tempat_lahir']. ', ' .$getter['tgl_lahir'],
			'role'=>$getter['role'],
			'alamat'=>$getter['alamat'],
			'telp'=>$getter['no_telp'],
			'jenisKel'=>$getter['jenis_klmn'],
			'foto'=>$getter['foto']);
		// $this->load->view('guru/per_guru');
		echo json_encode($out);
	}

	public function perAdmin($no){
		$getter = $this->User_model->_getUserData($no,'1');
		$out = array('no_induk'=>$getter['no_induk'],
			'nama_lengkap'=>$getter['nama_lengkap'],
			'ttl'=>$getter['tempat_lahir']. ', ' .$getter['tgl_lahir'],
			'role'=>$getter['role'],
			'alamat'=>$getter['alamat'],
			'telp'=>$getter['no_telp'],
			'jenisKel'=>$getter['jenis_klmn'],
			'foto'=>$getter['foto']);
		// $this->load->view('guru/per_guru');
		echo json_encode($out);
	}

	public function hapus(){
		$_POST = json_decode(file_get_contents('php://input'),true);
		$role = $_POST['role'];
		$id = $_POST['no_induk'];
		switch ($role) {
			case '1':
				$out = $this->User_model->_delUser($id, 'admin_tbl');
				if ($out) {
					echo json_encode(array('msg'=>'success'));
				} else{
					echo json_encode(array('msg'=>'err'));
				}
				break;
			
			case '2':
				$out = $this->User_model->_delUser($id, 'guru_tbl');
				if ($out) {
					echo json_encode(array('msg'=>'success'));
				} else{
					echo json_encode(array('msg'=>'err'));
				}
				break;
			case '3':
				$out = $this->User_model->_delUser($id, 'murid_tbl');
				if ($out) {
					echo json_encode(array('msg'=>'success'));
				} else{
					echo json_encode(array('msg'=>'err'));
				}
				break;
		}
	}

	public function chgPass($no_induk){
		$this->form_validation->set_rules('password', 'Password', 'trim|matches[cpassword]|required');
        $this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required');
		$_POST = json_decode(file_get_contents('php://input'),true);
		$input = array('u_password'=> $_POST['password']);

		if ($this->form_validation->run()==false) {
			$out = validation_errors();
			echo json_encode(array('res'=> $out,'msg'=>'err'));
		} else{
		
			$set = $this->User_model->_updatePass_Non_Profile($no_induk,$input);
			if ($set) {
				$out = array('msg'=>'err','res'=>'Ada sebuah error!');
				echo json_encode($out);
			} else {
				$out = array('msg'=>'success','res'=>'Ganti password sukses!');
				echo json_encode($out);
			}
		}
	}

	public function permurid($no){
		$getter = $this->User_model->_getUserData($no,'3');
		$out = array('no_induk'=>$getter['no_induk'],
			'nama_lengkap'=>$getter['nama'],
			'wali'=>$getter['nama_wali'],
			'ttl'=>$getter['tempat_lahir']. ', ' .$getter['tgl_lahir'],
			'role'=>$getter['role'],
			'alamat'=>$getter['alamat'],
			'telp'=>$getter['no_telp'],
			'jenisKel'=>$getter['jenis_klmn'],
			'foto'=>$getter['foto']);
		// $this->load->view('guru/per_guru');
		echo json_encode($out);
	}

	public function perGuru_view(){
		$this->load->view('guru/per_guru');
	}

	public function permurid_view(){
		$this->load->view('murid/per_murid');	
	}
	public function peradmin_view(){
		$this->load->view('admin/per_admin');	
	}

	public function getGuru(){
		$getter = $this->User_model->getAll('guru_tbl');

		echo json_encode($getter);

	}

	public function getMurid(){
		$getter = $this->User_model->getAll('murid_tbl');

		echo json_encode($getter);		
	}
	public function getAdmin(){
		$sess = $this->session->userdata('uid');
		$getter = $this->User_model->getAll_admin('admin_tbl',$sess);

		echo json_encode($getter);		
	}

	public function addUser(){
		// $_POST = json_decode(file_get_contents('php://input'),true);
		$role = $_POST['role'];

		echo json_encode(array('aa'=> $_FILES));
		print_r($_FILES);

		switch ($role) {
			case '1':
				$inputUser = array('no_induk'=> '',
						'u_password'=> '',
						'role'=>'' );
				$inputTable = array('no_induk' =>'' ,
						'nama_lengkap'=> '',
						'alamat'=> '',
						'no_telp'=>'' );
				$this->User_model->_addUser('admin_tbl',$inputUser,$inputTable);
				break;
			
			case '2':
			$this->form_validation->set_rules('no_induk','No Induk','required|is_unique[user_table.no_induk]', array('required'=>'Kolom %s harus diisi','is_unique'=>'%s sudah dipakai'));
			$this->form_validation->set_rules('password','Password','required', array('required'=>'Kolom %s harus diisi'));
			$this->form_validation->set_rules('nama_lengkap','Nama Lengkap','required', array('required'=>'Kolom %s harus diisi'));
			$this->form_validation->set_rules('alamat','Alamat','required', array('required'=>'Kolom %s harus diisi'));
			$this->form_validation->set_rules('telp','Nomor Telepon','required', array('required'=>'Kolom %s harus diisi'));
			$this->form_validation->set_rules('jenisK','Jenis Kelamin','required', array('required'=>'Kolom %s harus diisi'));

				if ($this->form_validation->run()==false) {
					$out = array('msg'=>'err','res'=>validation_errors());
					echo json_encode($out);
				}else {

					if (!(isset($_FILES['foto']))) {
						$inputUser = array('no_induk'=> $_POST['no_induk'],
							'u_password'=> $_POST['password'],
							'role'=>$role );
						$inputTable = array('no_induk' =>$_POST['no_induk'] ,
								'nama_lengkap'=> $_POST['nama_lengkap'],
								'alamat'=> $_POST['alamat'],
								'no_telp'=>$_POST['telp'],
								'jenis_klmn'=>$_POST['jenisK'] ,
								'foto'=>'' );
						$this->User_model->_addUser('guru_tbl',$inputUser,$inputTable);	
						$out = array('msg'=>'success','res'=>'Berhasil ditambahkan no img');
						echo json_encode($out);
					} else{

						$foto = $this->do_upload();
						if ($foto ==false) {
							echo json_encode(array('msg'=>'err','res'=>$foto));
						} else{

							$inputUser = array('no_induk'=> $_POST['no_induk'],
									'u_password'=> $_POST['password'],
									'role'=>$role );
							$inputTable = array('no_induk' =>$_POST['no_induk'] ,
									'nama_lengkap'=> $_POST['nama_lengkap'],
									'alamat'=> $_POST['alamat'],
									'no_telp'=>$_POST['telp'],
									'jenis_klmn'=>$_POST['jenisK'] ,
									'foto'=>$foto );
							$this->User_model->_addUser('guru_tbl',$inputUser,$inputTable);	
							$out = array('msg'=>'success','res'=>'Berhasil ditambahkan');
							echo json_encode($out);
							}
						}
					}

				break;

			case '3':

				break;
		}
	}

	public function do_upload(){
		// $config = array(
		// 	'upload_path' => "./ven/upload/",
		// 	'allowed_types' => "jpg|png|jpeg",
		// 	'file_ext_tolower'=>true,
		// 	'remove_spaces'=>true,
		// 	'encrypt_name'=>true,
		// 	'max_size' => "1024000", 
		// 	'max_height' => "800",
		// 	'max_width' => "800"
		// 	);
		// $this->load->library('upload', $config);
		// if($this->upload->do_upload($_FILES))
		// 	{
		// 		return $data = $this->upload->data();
		// 	}
		// 	else
		// 	{
		// 		return $error = $this->upload->display_errors();
		// 	}
		$errors= array();        
	    $file_name = $_FILES['foto']['name'];
	    $file_size =$_FILES['foto']['size'];
	    $file_tmp =$_FILES['foto']['tmp_name'];
	    $file_type=$_FILES['foto']['type'];   
	    $file_ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
	    if(empty($errors)==true){
        move_uploaded_file($file_tmp,"./ven/upload/".$file_name);
        return 'ven/upload/'.$file_name;
	    }else{
	        print_r($errors);
	    }
	}

}