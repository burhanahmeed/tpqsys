<!DOCTYPE html>
<html ng-app="admin">
<head lang="en-us">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title ng-bind="header"></title>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url()?>ven/libs/styles-dashboard.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>ven/css/admin-style.css">

<!--Icons-->
<script src="<?php echo base_url()?>app/js/libs/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body >
<script>var BASE_URL = "<?php echo base_url(); ?>";</script>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Sistem Informasi<span>TPQ</span></a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> <?php echo $name ?> <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a ui-sref="profile"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
							<li><a href="<?php echo BASE_URL('login/logout')?>"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Keluar</a></li>
						</ul>
					</li>
				</ul>
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>
		
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar coll-on-mobile">
		<ul class="nav menu">
			<li ui-sref-active="active">
			<a ui-sref="dashboard"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Dashboard</a>
			</li>
			<li ui-sref-active="active">
			<a ui-sref="manajemen_guru"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg> Manajemen Guru</a>
			</li>
			<li ui-sref-active="active">
			<a ui-sref="manajemen_murid"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Manajemen Murid</a></li>
			<li ui-sref-active="active">
			<a ui-sref="manajemen_admin"><svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg> Admin</a>
			</li>

			
			<li role="presentation" class="divider"></li>
			<li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Login Page</a></li>
		</ul>
		
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<!-- angular view -->
	<ui-view></ui-view>

	</div>	<!--/.main-->
	  

	<script src="<?php echo base_url()?>ven/libs/jquery.min.js"></script> 
	<script src="<?php echo base_url()?>ven/libs/angular.min.js"></script>
	<script src="<?php echo base_url()?>ven/libs/angular-ui-router.min.js"></script>
	<script src="<?php echo base_url()?>ven/libs/dirPagination.js"></script>
	<script src="<?php echo base_url()?>ven/libs/angular-resource.min.js"></script>
	<script src="<?php echo base_url()?>ven/libs/ng-file-upload-shim.min.js"></script>
	<script src="<?php echo base_url()?>ven/libs/ng-file-upload.min.js"></script>
	<script src="<?php echo base_url()?>ven/libs/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>ven/front/adminRoute.js"></script>

	<!-- CONTROLLER -->
	<script src="<?php echo base_url()?>ven/front/adminController.js"></script>

	<!-- DIRECTIVE -->
	<script src="<?php echo base_url()?>ven/front/adminDirective.js"></script>

	<!-- SERVICE -->
	<script src="<?php echo base_url()?>ven/front/adminService.js"></script>
	
</body>

</html>