<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>ven/libs/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>ven/css/style.css">
	<title></title>
</head>
<body ng-app="logApps">

			<div class="container" ng-controller="loginCtrl">    
		        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
		            <div class="panel panel-info" >
		                    <div class="panel-heading">
		                        <div class="panel-title">Sign In</div>
		                        <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div>
		                    </div>     

		                    <div style="padding-top:30px" class="panel-body" >

		                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
		                            
		                        <form id="loginform" class="form-horizontal" name="login" role="form" ng-submit="loginSubmit()" method="POST">
		                                    
		                            <div style="margin-bottom: 25px" class="input-group">
		                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
		                                        <input id="login-username" type="text" class="form-control" name="username" ng-model="username" value="" placeholder="No Induk" required="">                                        
		                                    </div>
		                                
		                            <div style="margin-bottom: 25px" class="input-group">
		                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
		                                        <input id="login-password" type="password" class="form-control" ng-model="password" name="password" placeholder="password" required="">
		                                    </div>
		                                    

						<div id="notif"><div ng-show="loading" class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-repeat slow-right-spinner"> LOADING</div></div>
		                                
		                            <div class="input-group">
		                                      <div class="checkbox">
		                                        <label>
		                                          <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
		                                        </label>
		                                      </div>
		                                    </div>


		                                <div style="margin-top:10px" class="form-group">
		                                    <!-- Button -->

		                                    <div class="col-sm-12 controls">
		                                      <button ng-disabled="login.$invalid" type="submit" class="btn btn-success">Masuk</button>

		                                    <!--  <input type="submit" name="submit" value="Login" ng-disabled="login.$invalid"> -->
		                                      <a id="btn-fblogin" class="btn btn-primary">Login with Facebook</a>

		                                    </div>
		                                </div>


		                                <div class="form-group">
		                                    <div class="col-md-12 control">
		                                        <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
		                                            Don't have an account! 
		                                        <a href="#" onClick="$('#loginbox').hide(); $('#signupbox').show()">
		                                            Sign Up Here
		                                        </a>
		                                        </div>
		                                    </div>
		                                </div>    
		                            </form>     



		                        </div>                     
		                    </div>  
		        </div>        
		              
		         </div> 
		    </div>
    

</body>

<!-- Javascript library -->
<script type="text/javascript" src="<?php echo base_url()?>ven/libs/angular.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>ven/libs/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>ven/libs/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>ven/front/loginApps.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>ven/js/script.js"></script>

</html>