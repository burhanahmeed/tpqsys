<!-- <div class="container"> -->
<div class="row offset-atas">
    <div class="link-table" style="margin-left: 30px;"><h3>Daftar Murid</h3>
    </div>
            <div class="col-md-12 link-table">
                <div class="panel panel-primary">
                    <div style="background-color: #30a5ff; border:none;" class="panel-heading panel-head-min">
                        <h3 class="panel-title">Murid</h3>
                        <div class="pull-right">
                            <span class="clickable filter" data-toggle="tooltip" title="Toggle table filter" data-container="body">
                                <i class="glyphicon glyphicon-user"></i>
                            </span>
                        </div>
                    </div>
                    <div class="panel-body">
                   
                    </div>
                         <form class="form-inline" >
                            <div class="form-group" style="margin: 5px 10px 5px 10px;">
                                <label >Cari</label>
                                <input type="text" ng-model="search" class="form-control" placeholder="Search">
                            </div>
                        </form>
                        <!-- pager -->
                        <div class="center">
                            <ul class="pagination" style="margin: 0 0 0 0;">
                            <dir-pagination-controls
                               max-size="10"
                               direction-links="true"
                               boundary-links="true" >
                            </dir-pagination-controls>
                            </ul>
                        </div>
                        <form method="post" name="ckhbox">
                         <button class="btn-min add" data-target="#add-data" data-toggle="modal">Tambah Murid</button>
                         <button class="btn-min delete" ng-disabled="ckhbox.$invalid" ng-click="delMultiple()">Hapus Centang</button>
                        
                        <div class="table-responsive">
                        <table class="table table-hover" id="dev-table">
                        <thead>
                            <tr>
                                <th><input type="checkbox" id="checkBoxAll"></th>
                                <th>#No</th>
                                <th><a ng-click="sortBy('short_url')">Nomor Induk</a><span class="sortorder" ng-show="propertyName === 'short_url'" ng-class="{reverse: reverse}"></span></th>
                                <th>Nama</th>
                                <th>Nama Wali</th>
                                <th>Jenis Kelamin</th>
                                <th>Alamat</th>
                                <th>Telepon</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr dir-paginate="a in murids| orderBy:propertyName:reverse | filter:search | itemsPerPage:50">
                                <td><input type="checkbox" class="checkBoxId" value="{{link.id}}" ng-model="a.selected"></td>
                                <td>{{$index +1}}</td>
                                <td><a ui-sref="perMurid({no_induk:a.no_induk})">{{a.no_induk}}</a></td>
                                <td>{{a.nama}}</td>
                                <td>{{a.nama_wali}}</td>
                                <td>{{a.jenis_klmn}}</td>
                                <td>{{a.alamat}}</td>
                                <td>{{a.no_telp}}</td>
                            </tr>
                        </tbody>
                        </form>
                    </table>
                    </div>
                    <!-- pager -->
                    <div class="center">
                         <ul class="pagination">
                        <dir-pagination-controls
                           max-size="10"
                           direction-links="true"
                           boundary-links="true" >
                        </dir-pagination-controls>
                        </ul>
                    </div>
                </div>
            </div>
</div>
    <!-- </div> -->

    <!-- Edit Modal -->

    <div class="modal fade" id="add-data" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <form method="POST" name="editItem" role="form" ng-submit="addGuru(picFile)">
                  <!--   <input ng-model="editc.id" type="hidden" placeholder="Name" name="inputId" class="form-control" /> -->
                    <!-- <input ng-model="editc.long_url" type="hidden" placeholder="Name" name="inputUrl" class="form-control" /> -->
                <div class="modal-header color">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Link</h4>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <label>Foto</label>
                               <input type="file" ngf-select ng-model="picFile" name="file"    
             accept="image/*" ngf-max-size="1MB" 
             ngf-model-invalid="errorFile">
      <i ng-show="myForm.file.$error.maxSize">File too large 
          {{errorFile.size / 1000000|number:1}}MB: max 1M</i>
      <div style="width: 200px;" class="thum"><img ng-show="myForm.file.$valid" ngf-thumbnail="picFile" class="thumb"></div> <button class="rem" ng-click="picFile = null" ng-show="picFile"><span class="glyphicon glyphicon-remove"></span> Remove</button><br>
                                <label>Nomor Induk</label><br>
                                <input class="input-add" type="text" ng-model="no_induk" autofocus><br>
                                <label>Password</label><br>
                                <input class="input-add" type="password" ng-model="password"><br>
                                <label>Role</label><br>
                                <input class="input-add" type="text" value="Administrator" disabled=""><br>
                                <label>Nama Lengkap</label><br>
                                <input class="input-add" type="text" ng-model="namaGuru"><br>
                                <label>Alamat</label><br>
                                <input class="input-add" type="text" ng-model="alamatGuru"><br>
                                <label>Nomor Telepon</label><br>
                                <input class="input-add" type="text" ng-model="telpGuru"><br>
                                <label>Jenis Kelamin</label><br>
                                <select class="input-add" ng-model="jenisKel">
                                    <option ng-repeat="j in jenisK" value="{{j.nama}}">{{j.nama}}</option>
                                </select><br>
                            </div>
                        </div>
                        <div class="form-group">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" ng-disabled="editItem.$invalid" class="btn btn-primary create-crud">Submit</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div id="notif"></div>