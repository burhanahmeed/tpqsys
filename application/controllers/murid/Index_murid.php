<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index_murid extends CI_Controller {
//index of MURID

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Login_model');
	}

	public function index()
	{
		$data = array(
			'name'=>$this->session->userdata('login')['nama']
			);
		$this->load->view('murid/template',$data);
	}

}