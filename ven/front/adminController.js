/*
CREATED BY MED

*/

admin.controller('profileCtrl',function($rootScope, $scope, $http) { 
  $rootScope.header = "Halaman Profil"; 

  $http.get(BASE_URL+'profile')
    .success(function(data){
      // console.log(data);
      $scope.profile = data;
    });

     $scope.editsubmit = function() {
       $http({
          method:'POST',
          url:BASE_URL+'Profile/editProfile',
          data:$scope.profile,
          header:{ 'Content-Type': 'application/x-www-form-urlencoded' } 
       }).success(function(data){
        console.log(data);
        var obj = $.parseJSON(JSON.stringify(data));
        if(obj['msg']=='success'){
          $(".modal").modal("hide");
          $('#notif').show().html('<div class="alert alert-success">'+
            '<strong>Success!</strong> '+obj['res']+'.</div>').delay( 1000 ).hide( 400 );
        } 
        else if (obj['msg']=='err') {
           $('#notif').show().html('<div class="alert alert-warning">'+
            '<strong>Warning!</strong> '+obj['res']+'.</div>').delay( 1000 ).hide( 400 );
        }

        $scope.refresh();
       });
    }

    $scope.refresh = function(){
    $http.get(BASE_URL+'link_management/list_links')
          .success(function(data){
               $scope.links = data;
      });
    }

    $scope.changePass = function(){
    $http({
          method:'POST',
          url:BASE_URL+'profile/editPass/',
          data:{'password': $scope.password,'cpassword':$scope.cpassword},
          header:{ 'Content-Type': 'application/x-www-form-urlencoded' } 
       }).success(function(data){
        console.log($scope.password);
        // $(".modal").modal("hide");
        var obj = $.parseJSON(JSON.stringify(data));
        if (obj['msg']=='success') {
          $('#notif').show().html('<div class="alert alert-success">'+
            '<strong>Success!</strong> '+obj['res']+'.</div>').delay( 1000 ).hide( 400 );
          // if (delay==1000) {location.reload();}
          setTimeout(function(){location.reload()},1500);
        } else if (obj['msg']=='err') {
          $('#notif').show().html('<div class="alert alert-warning">'+
            '<strong>Warning!</strong> '+obj['res']+'.</div>').delay( 1000 ).hide( 400 );
        }

      });
  }

});//profile

admin.controller('guruCtrl', function($rootScope, $scope, $http, Upload) { 
  $rootScope.header = "Manajemen Guru";
  $scope.gurus = [];
  $http.get(BASE_URL+'admin/Manajemen_sdm/getGuru')
    .success(function(data){
      var guru = data;
      // console.log(data);
       // $scope.otherCondition = true;
    $scope.propertyName = 'id';
    $scope.reverse = true;
    $scope.gurus = guru;

    $scope.sortBy = function(propertyName) {
      $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
      $scope.propertyName = propertyName;
    };
    });

    $scope.itemsPerPage = 50;
    $scope.currentPage = 0;
    $scope.datalists = $scope.gurus ; 

    $scope.range = function() {
     var rangeSize = 6;
     var ps = [];
     var start;

     start = $scope.currentPage;

     if ( start > $scope.pageCount()-rangeSize ) {
      start = $scope.pageCount()-rangeSize+1;
      }
     for (var i=start; i<start+rangeSize; i++) {
     ps.push(i);
    }
    return ps;
    };
    $scope.prevPage = function() {
    if ($scope.currentPage > 0) {
    $scope.currentPage--;
    }
    };

    $scope.DisablePrevPage = function() {
    return $scope.currentPage === 0 ? "disabled" : "";
    };

    $scope.pageCount = function() {
    return Math.ceil($scope.datalists.length/$scope.itemsPerPage)-1;
    };

    $scope.nextPage = function() {
    if ($scope.currentPage > $scope.pageCount()) {
    $scope.currentPage++;
    }
    };

    $scope.DisableNextPage = function() {
    return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
    };

    $scope.setPage = function(n) {
    $scope.currentPage = n;
    };

   $scope.editLink = function($id){
    // $scope.editc = [];
      $http.get('link_management/get_edit_data/' + $id)
      .success(function(data){
        $scope.editc = data;
      })
   }

   $scope.addGuru = function(file){
    var data = {
 
         'foto': file,
 
         'no_induk':$scope.no_induk,
 
         'password':$scope.password,
 
         'nama_lengkap':$scope.namaGuru,
 
         'alamat':$scope.alamatGuru,
 
         'telp':$scope.telpGuru,
 
         'jenisK':$scope.jenisKel,
 
         'role': '2'};
 
 
    // var uploadUrl = BASE_URL+'admin/Manajemen_sdm/addUser';
 
    // multipartForm.posts(uploadUrl, data)
 
    // .success(function(datas){
 
    //   console.log(datas);
 
    //   console.log(data);
 
    // });
    file.upload = Upload.upload({
      url: BASE_URL+'admin/Manajemen_sdm/addUser',
      data: data,
    });

    file.upload.then(function (data) {

        console.log(data);    
    });
  }
 

   $scope.jenisK = [
        {nama:'Laki-laki'},
        {nama:'Perempuan'}]

    $scope.delMultiple = function(){
      $scope.itemList = [];
      angular.forEach($scope.links, function(link){
        if (!!link.selected) {
          $scope.itemList.push(link.id)
        }
      });
      var res = confirm('Are you sure want to delete it?');
      if (res) {
      $http.post(BASE_URL+'link_management/delMultiple/',{'id':$scope.itemList})
      .success(function(data){
        // console.log($scope.itemList);
        // console.log(data);
        $scope.refresh();
      });
      }
    }
    $scope.refresh = function(){
    $http.get(BASE_URL+'admin/Manajemen_sdm/getGuru')
          .success(function(data){
               $scope.gurus = data;
      });
    }

}); //guru Function

admin.run(function($rootScope, $location, $anchorScroll, $stateParams, $timeout) { 
  $rootScope.$on('$stateChangeSuccess', function(newRoute, oldRoute) {
    $timeout(function() { 
      $location.hash($stateParams.no_induk);
      $anchorScroll()
    }, 100)
  });
})

admin.controller('muridCtrl', function($rootScope, $scope, $http) { 
  $rootScope.header = "Manajemen Murid";
  $scope.murids = [];
  $http.get(BASE_URL+'admin/Manajemen_sdm/getMurid')
    .success(function(data){
      var murid = data;
      // console.log(data);
       // $scope.otherCondition = true;
    $scope.propertyName = 'id';
    $scope.reverse = true;
    $scope.murids = murid;

    $scope.sortBy = function(propertyName) {
      $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
      $scope.propertyName = propertyName;
    };
    });

    $scope.itemsPerPage = 50;
    $scope.currentPage = 0;
    $scope.datalists = $scope.murids ; 

    $scope.range = function() {
     var rangeSize = 6;
     var ps = [];
     var start;

     start = $scope.currentPage;

     if ( start > $scope.pageCount()-rangeSize ) {
      start = $scope.pageCount()-rangeSize+1;
      }
     for (var i=start; i<start+rangeSize; i++) {
     ps.push(i);
    }
    return ps;
    };
    $scope.prevPage = function() {
    if ($scope.currentPage > 0) {
    $scope.currentPage--;
    }
    };

    $scope.DisablePrevPage = function() {
    return $scope.currentPage === 0 ? "disabled" : "";
    };

    $scope.pageCount = function() {
    return Math.ceil($scope.datalists.length/$scope.itemsPerPage)-1;
    };

    $scope.nextPage = function() {
    if ($scope.currentPage > $scope.pageCount()) {
    $scope.currentPage++;
    }
    };

    $scope.DisableNextPage = function() {
    return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
    };

    $scope.setPage = function(n) {
    $scope.currentPage = n;
    };

    $scope.addGuru = function(file){
    var data = {
 
         'foto': file,
 
         'no_induk':$scope.no_induk,
 
         'password':$scope.password,
 
         'nama_lengkap':$scope.namaGuru,
 
         'alamat':$scope.alamatGuru,
 
         'telp':$scope.telpGuru,
 
         'jenisK':$scope.jenisKel,
 
         'role': '3'};
 
 
    // var uploadUrl = BASE_URL+'admin/Manajemen_sdm/addUser';
 
    // multipartForm.posts(uploadUrl, data)
 
    // .success(function(datas){
 
    //   console.log(datas);
 
    //   console.log(data);
 
    // });
    file.upload = Upload.upload({
      url: BASE_URL+'admin/Manajemen_sdm/addUser',
      data: data,
    });

    file.upload.then(function (data) {

        console.log(data);    
    });
  }

     $scope.jenisK = [
        {nama:'Laki-laki'},
        {nama:'Perempuan'}]

    $scope.delMultiple = function(){
      $scope.itemList = [];
      angular.forEach($scope.links, function(link){
        if (!!link.selected) {
          $scope.itemList.push(link.id)
        }
      });
      var res = confirm('Are you sure want to delete it?');
      if (res) {
      $http.post(BASE_URL+'link_management/delMultiple/',{'id':$scope.itemList})
      .success(function(data){
        // console.log($scope.itemList);
        // console.log(data);
        $scope.refresh();
      });
      }
    }
    $scope.refresh = function(){
    $http.get(BASE_URL+'admin/Manajemen_sdm/getMurid')
          .success(function(data){
               $scope.murids = data;
      });
    }

}); //murid Function

admin.controller('admCtrl', function($rootScope, $scope, $http, multipartForm) { 
  $rootScope.header = "Manajemen Admin";
  $scope.adm = [];
  $http.get(BASE_URL+'admin/Manajemen_sdm/getAdmin')
    .success(function(data){
      var admin = data;
      // console.log(data);
       // $scope.otherCondition = true;
    $scope.propertyName = 'id';
    $scope.reverse = true;
    $scope.adm = admin;

    $scope.sortBy = function(propertyName) {
      $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
      $scope.propertyName = propertyName;
    };
    });

    $scope.itemsPerPage = 50;
    $scope.currentPage = 0;
    $scope.datalists = $scope.adm ; 

    $scope.range = function() {
     var rangeSize = 6;
     var ps = [];
     var start;

     start = $scope.currentPage;

     if ( start > $scope.pageCount()-rangeSize ) {
      start = $scope.pageCount()-rangeSize+1;
      }
     for (var i=start; i<start+rangeSize; i++) {
     ps.push(i);
    }
    return ps;
    };
    $scope.prevPage = function() {
    if ($scope.currentPage > 0) {
    $scope.currentPage--;
    }
    };

    $scope.DisablePrevPage = function() {
    return $scope.currentPage === 0 ? "disabled" : "";
    };

    $scope.pageCount = function() {
    return Math.ceil($scope.datalists.length/$scope.itemsPerPage)-1;
    };

    $scope.nextPage = function() {
    if ($scope.currentPage > $scope.pageCount()) {
    $scope.currentPage++;
    }
    };

    $scope.DisableNextPage = function() {
    return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
    };

    $scope.setPage = function(n) {
    $scope.currentPage = n;
    };

      $scope.addGuru = function(file){
    var data = {
 
         'foto': file,
 
         'no_induk':$scope.no_induk,
 
         'password':$scope.password,
 
         'nama_lengkap':$scope.namaGuru,
 
         'alamat':$scope.alamatGuru,
 
         'telp':$scope.telpGuru,
 
         'jenisK':$scope.jenisKel,
 
         'role': '1'};
 
 
    // var uploadUrl = BASE_URL+'admin/Manajemen_sdm/addUser';
 
    // multipartForm.posts(uploadUrl, data)
 
    // .success(function(datas){
 
    //   console.log(datas);
 
    //   console.log(data);
 
    // });
    file.upload = Upload.upload({
      url: BASE_URL+'admin/Manajemen_sdm/addUser',
      data: data,
    });

    file.upload.then(function (data) {

        console.log(data);    
    });
  }

 

   $scope.jenisK = [
        {nama:'Laki-laki'},
        {nama:'Perempuan'}]

    $scope.delMultiple = function(){
      $scope.itemList = [];
      angular.forEach($scope.links, function(link){
        if (!!link.selected) {
          $scope.itemList.push(link.id)
        }
      });
      var res = confirm('Are you sure want to delete it?');
      if (res) {
      $http.post(BASE_URL+'link_management/delMultiple/',{'id':$scope.itemList})
      .success(function(data){
        // console.log($scope.itemList);
        // console.log(data);
        $scope.refresh();
      });
      }
    }
    $scope.refresh = function(){
    $http.get(BASE_URL+'admin/Manajemen_sdm/getAdmin')
          .success(function(data){
               $scope.adm = data;
      });
    }


}); //admin Function

admin.filter('pagination', function()
{
  return function(input, start) {
    start = parseInt(start, 10);
    return input.slice(start);
  };
});

//BAGIAN PER PER

admin.controller('perguruCtrl', function($rootScope, $scope, $http,$stateParams) { 
  $rootScope.header = $scope.no_induk+' | Guru';
  $scope.perguru = [];
  $scope.no_induk = $stateParams.no_induk; 
  $http.get(BASE_URL+'admin/Manajemen_sdm/perGuru/'+$scope.no_induk)
    .success(function(data){
     $scope.perguru = data;
     console.log(data);
  });

  $scope.jenisK = [
        {nama:'Laki-laki'},
        {nama:'Perempuan'}];

      $scope.delete = function($id,$role){
      var res = confirm('Yakin akan menghapus akun guru ini?');
      if(res){
      $http.post(BASE_URL+'admin/Manajemen_sdm/hapus/',{no_induk: $id, role: $role})
      .success(function(){
        // $scope.links.splice($id,1);
        setTimeout(function(){location.replace(BASE_URL+'admin/index_admin#/manajemen_guru')},1000);
        $scope.refresh();
        console.log('berhasil Di delete ');
      });
      }
    }

    $scope.refresh = function(){
    $http.get(BASE_URL+'admin/Manajemen_sdm/getGuru')
          .success(function(data){
               $scope.gurus = data;
      });
    }

    $scope.changePass = function($no_induk){
    $http({
          method:'POST',
          url:BASE_URL+'admin/Manajemen_sdm/chgPass/'+$no_induk,
          data:{'password': $scope.password,'cpassword':$scope.cpassword},
          header:{ 'Content-Type': 'application/x-www-form-urlencoded' } 
       }).success(function(data){
        console.log($scope.password);
        // $(".modal").modal("hide");
        var obj = $.parseJSON(JSON.stringify(data));
        if (obj['msg']=='success') {
          $('#notif').show().html('<div class="alert alert-success">'+
            '<strong>Success!</strong> '+obj['res']+'.</div>').delay( 1000 ).hide( 400 );
          // if (delay==1000) {location.reload();}
          setTimeout(function(){location.reload()},1500);
        } else if (obj['msg']=='err') {
          $('#notif').show().html('<div class="alert alert-warning">'+
            '<strong>Warning!</strong> '+obj['res']+'.</div>').delay( 1000 ).hide( 400 );
        }

      });
  }

}); //per guru Function

admin.controller('permuridCtrl', function($rootScope, $scope, $http,$stateParams){
  $rootScope.header = $scope.no_induk+' | Murid';
  $scope.permurid = [];
  $scope.no_induk = $stateParams.no_induk; 
  $http.get(BASE_URL+'admin/Manajemen_sdm/permurid/'+$scope.no_induk)
    .success(function(data){
     $scope.permurid = data;
     console.log(data);
  });

    $scope.jenisK = [
        {nama:'Laki-laki'},
        {nama:'Perempuan'}];

      $scope.delete = function($id,$role){
      var res = confirm('Yakin akan menghapus akun guru ini?');
      if(res){
      $http.post(BASE_URL+'admin/Manajemen_sdm/hapus/',{no_induk: $id, role: $role})
      .success(function(){
        // $scope.links.splice($id,1);
        $scope.refresh();
        console.log('berhasil Di delete ');
      });
      }
    }

    $scope.changePass = function($no_induk){
    $http({
          method:'POST',
          url:BASE_URL+'admin/Manajemen_sdm/chgPass/'+$no_induk,
          data:{'password': $scope.password,'cpassword':$scope.cpassword},
          header:{ 'Content-Type': 'application/x-www-form-urlencoded' } 
       }).success(function(data){
        console.log($scope.password);
        // $(".modal").modal("hide");
        var obj = $.parseJSON(JSON.stringify(data));
        if (obj['msg']=='success') {
          $('#notif').show().html('<div class="alert alert-success">'+
            '<strong>Success!</strong> '+obj['res']+'.</div>').delay( 1000 ).hide( 400 );
          // if (delay==1000) {location.reload();}
          setTimeout(function(){location.reload()},1500);
        } else if (obj['msg']=='err') {
          $('#notif').show().html('<div class="alert alert-warning">'+
            '<strong>Warning!</strong> '+obj['res']+'.</div>').delay( 1000 ).hide( 400 );
        }

      });
  }
  
});

admin.controller('peradminCtrl', function($rootScope, $scope, $http,$stateParams){
  $rootScope.header = $scope.no_induk+' | Admin';
  $scope.peradmin = [];
  $scope.no_induk = $stateParams.no_induk; 
  $http.get(BASE_URL+'admin/Manajemen_sdm/perAdmin/'+$scope.no_induk)
    .success(function(data){
     $scope.peradmin = data;
     console.log(data);
  });

    $scope.jenisK = [
        {nama:'Laki-laki'},
        {nama:'Perempuan'}];

      $scope.delete = function($id,$role){
      var res = confirm('Yakin akan menghapus akun guru ini?');
      if(res){
      $http.post(BASE_URL+'admin/Manajemen_sdm/hapus/',{no_induk: $id, role: $role})
      .success(function(){
        // $scope.links.splice($id,1);
        $scope.refresh();
        console.log('berhasil Di delete ');
      });
      }
    }

    $scope.changePass = function($no_induk){
    $http({
          method:'POST',
          url:BASE_URL+'admin/Manajemen_sdm/chgPass/'+$no_induk,
          data:{'password': $scope.password,'cpassword':$scope.cpassword},
          header:{ 'Content-Type': 'application/x-www-form-urlencoded' } 
       }).success(function(data){
        console.log($scope.password);
        // $(".modal").modal("hide");
        var obj = $.parseJSON(JSON.stringify(data));
        if (obj['msg']=='success') {
          $('#notif').show().html('<div class="alert alert-success">'+
            '<strong>Success!</strong> '+obj['res']+'.</div>').delay( 1000 ).hide( 400 );
          // if (delay==1000) {location.reload();}
          setTimeout(function(){location.reload()},1500);
        } else if (obj['msg']=='err') {
          $('#notif').show().html('<div class="alert alert-warning">'+
            '<strong>Warning!</strong> '+obj['res']+'.</div>').delay( 1000 ).hide( 400 );
        }

      });
  }
  
});